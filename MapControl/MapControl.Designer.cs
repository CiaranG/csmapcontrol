//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010-2012, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 

namespace csmapcontrol
{
	partial class MapControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// MapControl
			// 
			this.DoubleBuffered = true;
			this.Name = "MapControl";
			this.Size = new System.Drawing.Size(447, 332);
			this.MouseLeave += new System.EventHandler(this.MapControlMouseLeave);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MapControlMouseMove);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MapControlMouseDown);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MapControlMouseUp);
			this.ResumeLayout(false);
		}

		private System.Windows.Forms.Timer timer1;

	}
}
