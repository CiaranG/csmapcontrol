//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010-2012, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;

namespace csmapcontrol
{
	public class Marker
	{
		public double lat;
		public double lon;

		/// <summary>
		/// The tag for this marker - used to identify it by whatever means the
		/// user deems suitable.
		/// </summary>
		public object Tag;
	}

	public class TileManager
	{

		public delegate void NewDataAvailableHandler();

		public event NewDataAvailableHandler NewDataAvailable;

		/// <summary>
		/// Dictionary of cached tiles, keyed on a string formed
		/// as "zoom,x,y".
		/// </summary>
		private Dictionary<string, Bitmap> mTiles;

		/// <summary>
		/// Tile currently being downloaded, or null if none.
		/// </summary>
		private string mDownloadTile = null;

		public TileManager()
		{
			mTiles = new Dictionary<string, Bitmap>();
		}

		public void EmptyCache()
		{
			foreach(string key in mTiles.Keys) {
				if(mTiles [key] != null)
					mTiles [key].Dispose();
			}
			mTiles.Clear();
		}

		public Image GetTile(int x, int y, int zoom)
		{
			lock(mTiles) {
				string key = zoom.ToString() + ',' + x.ToString() + ',' + y.ToString();
				if(mTiles.ContainsKey(key)) {
					// We already have the tile in our cache...
					return mTiles [key];
				}
				if(mDownloadTile == null) {
					// Start downloading the tile...
					mDownloadTile = key;
					Thread t = new Thread(Download);
					t.Start();
				}
				return null;
			}
		}

		private void Download()
		{
			try {
				string[] vals = mDownloadTile.Split(',');
				string url = "http://tile.openstreetmap.org/" + vals [0] + "/" + vals [1] + "/" + vals [2] + ".png";
				WebClient client = new WebClient();
				Stream stream = client.OpenRead(url);
				Bitmap tile = new Bitmap(stream);
				stream.Flush();
				stream.Close();
				lock(mTiles) {
					mTiles [mDownloadTile] = tile;
				}
				// TODO: If we've reached the (as yet undefined) limit of how
				// many tiles we should be caching, we should remove the least
				// recently used one.
				if(NewDataAvailable != null)
					NewDataAvailable();
			} catch(Exception) {
				lock(mTiles) {
					mTiles [mDownloadTile] = null;
				}
				if(NewDataAvailable != null)
					NewDataAvailable();
			} finally {
				mDownloadTile = null;
			}

		}

	}

	/// <summary>
	/// Map control
	/// </summary>
	public partial class MapControl : UserControl
	{

		/// <summary>
		/// A predetermined list of sensible (-ish) discrete scales that can be
		/// used, for example, to provide a linear zoom range for a scrollbar.
		/// </summary>
		public static double[] DiscreteScales = {
			0.00005 / 256,
			0.0005 / 256,
			0.0008 / 256,			
			0.002 / 256,
			0.002 / 256,
			0.003 / 256,
			0.004 / 256,
			0.005 / 256,
			0.006 / 256,
			0.007 / 256,
			0.008 / 256,		
			0.009 / 256,
			0.01 / 256,
			0.015 / 256,
			0.02 / 256,
			0.025 / 256,
			0.03 / 256,
			0.035 / 256,
			0.04 / 256,
			0.045 / 256,
			0.05 / 256,
			0.055 / 256,
			0.06 / 256,
			0.065 / 256,
			0.07 / 256,
			0.075 / 256,
			0.08 / 256,
			0.085 / 256,
			0.09 / 256,
			0.095 / 256,
			0.1 / 256,
            0.125 / 256,
            0.15 / 256,
            0.175 / 256,
            0.2 / 256,
            0.225 / 256,
            0.25 / 256,
            0.275 / 256,
			0.3 / 256,
            0.35 / 256,
			0.4 / 256,
            0.45 / 256,
			0.5 / 256,
			0.55 / 256,
			0.6 / 256,
			0.7 / 256,
			0.8 / 256,
			0.9 / 256,
			1.0 / 256,
			2.0 / 256,
			3.0 / 256,
			4.0 / 256,
			5.0 / 256,
			6.0 / 256,
			7.0 / 256,
			8.0 / 256,
			9.0 / 256,
			10.0 / 256,
			15.0 / 256,
			20.0 / 256,
			30.0 / 256,
			40.0 / 256,
			50.0 / 256,
			60.0 / 256,
			70.0 / 256,
			80.0 / 256,
			90.0 / 256,
			100.0 / 256,
			110.0 / 256,
			130.0 / 256,
			150.0 / 256,
			170.0 / 256,
			190.0 / 256,
			210.0 / 256,
			240.0 / 256,
			1};
		
		public List<Marker> Markers
		{
			get { return mMarkers; }
		}

		private List<Marker> mMarkers = new List<Marker>();

		public List<Track> Tracks
		{
			get { return mTracks; }
		}

		private List<Track> mTracks = new List<Track>();
		
		/// <summary>
		/// Latitude at centre of view
		/// </summary>
		public double Latitude
		{
			get { return mLat; }
			set {
				mLat = value;
				Invalidate();
			}
		}

		private double mLat = 53.9753;

		/// <summary>
		/// Longitude at centre of view
		/// </summary>
		public double Longitude
		{
			get { return mLon; }
			set {
				mLon = value;
				Invalidate();
			}
		}

		private double mLon = -1.7017;

		/// <summary>
		/// Scale - longitude degrees per pixel
		/// </summary>
		public double LonScale
		{
			get { return mScale; }
			set {
				mScale = value;
				Invalidate();
			}
		}

		private double mScale = 1.0 / 256;

		public bool HighlightEntities
		{
			get { return mHighlightEntities; }
			set {
				mHighlightEntities = value;
				Invalidate();
			}
		}

		private bool mHighlightEntities = false;
		private EntityInfo mHighlightedEntity = null;

		/// <summary>
		/// Tile cache
		/// </summary>
		private static TileManager mTileManager = new TileManager();
		private bool mUpdateView = false;

		/// <summary>
		/// Event raised when the view parameters are changed INTERNALLY.
		/// For example, by the user dragging the map. This event is not raised
		/// when the change is made by the client, e.g. by setting the relevant
		/// properties.
		/// </summary>
		public event ViewChangedEventHandler ViewChanged;
		public delegate void ViewChangedEventHandler(double lat,double lon,double scale);

		private void RaiseViewChanged()
		{
			if(ViewChanged != null) {
				ViewChanged(mLat, mLon, mScale);
			}
		}

		/// <summary>
		/// Event raised when there is diagnostics information available.
		/// </summary>
		public event DiagsInfoEventHandler DiagsInfo;
		public delegate void DiagsInfoEventHandler(string sMsg);

		private void Diags(string msg)
		{
			if(DiagsInfo != null)
				DiagsInfo(msg);
		}

		/// <summary>
		/// Event raised when the user clicks at a location on the map.
		/// </summary>
		public event LocationClickedEventHandler LocationClicked;
		public delegate void LocationClickedEventHandler(double lat,double lon);

		/// <summary>
		/// Event raised when the user clicks a Marker.
		/// </summary>
		public event MarkerClickedEventHandler MarkerClicked;
		public delegate void MarkerClickedEventHandler(Marker m);

		/// <summary>
		/// Event raised when the user clicks a Track point.
		/// </summary>
		public event TrackPointClickedEventHandler TrackPointClicked;
		public delegate void TrackPointClickedEventHandler(Track track,DateTime time);


		/// <summary>
		/// Clear the tile cache. Since the tile manager is static, and shared
		/// between all instances of the control, it will remain in place for
		/// the lifetime of the application unless this is called.
		/// </summary>
		public static void ClearTileCache()
		{
			if(mTileManager != null)
				mTileManager.EmptyCache();			
		}

		public MapControl()
		{
			InitializeComponent();
			mTileManager.NewDataAvailable += NewTileDataAvailable;
		}

        public EntityInfo GetHighlightedEntity()
        {
            return mHighlightedEntity;
        }

		/// <summary>
		/// Flag that the view needs updating. Calling this will cause a call
		/// to Invalidate() at the next timer tick. We need to go down this
		/// convoluted route rather than simply calling Invalidate() from
		/// the NewTileDataAvailable event handler, because that could cause
		/// it to be called when a paint is still in progress, and this means
		/// the Invalidate gets ignored.
		/// </summary>
		private void UpdateView()
		{
			mUpdateView = true;
		}

		private void NewTileDataAvailable()
		{
			if(!this.IsHandleCreated)
				return;
			if(InvokeRequired) {
				BeginInvoke(new MethodInvoker(delegate
				{
					UpdateView(); }));
			} else {
				UpdateView();
			}
		}

		/// <summary>
		/// Get the Tile X/Y position for a given latitude and longitude, and
		/// at a given zoom level.
		/// </summary>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		/// <param name="zoom">The zoom level</param>
		/// <param name="x">The tile X</param>
		/// <param name="y">The tile Y</param>
		private void GetTileXYFromLatLon(double lat, double lon, int zoom, out int x, out int y)
		{
			x = (int)((lon + 180.0) / 360.0 * Math.Pow(2.0, zoom));
			y = (int)((1.0 - Math.Log(Math.Tan(lat * Math.PI / 180.0) +
				1.0 / Math.Cos(lat * Math.PI / 180.0)) / Math.PI) / 2.0 * Math.Pow(2.0, zoom));
		}

		/// <summary>
		/// Get the latitude and longitude for the tile at the given X/Y and
		/// zoom. The lat/lon returned is for the top left corner of the tile.
		/// </summary>
		/// <param name="x">The tile X</param>
		/// <param name="y">The tile Y</param>
		/// <param name="zoom">The zoom level</param>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		private void GetLatLonFromTileXY(int x, int y, int zoom, out double lat, out double lon)
		{
			lon = ((x / Math.Pow(2.0, zoom) * 360.0) - 180.0);
			double n = Math.PI - ((2.0 * Math.PI * y) / Math.Pow(2.0, zoom));
			lat = (180.0 / Math.PI * Math.Atan(Math.Sinh(n)));
		}

		/// <summary>
		/// Get the lat/lon bounds of the current display.
		/// </summary>
		/// <param name="lat1"></param>
		/// <param name="lon1"></param>
		/// <param name="lat2"></param>
		/// <param name="lon2"></param>
		public void GetDisplayBounds(out double lat1, out double lon1, out double lat2, out double lon2)
		{
			ClientPosToLatLon(0, 0, out lat1, out lon1);
			ClientPosToLatLon(Width - 1, Height - 1, out lat2, out lon2);
		}

		/// <summary>
		/// Get the latitude and longitude of a given position in the client
		/// area.
		/// </summary>
		/// <param name="x">The X position</param>
		/// <param name="y">The Y position</param>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		private void ClientPosToLatLon(int x, int y, out double lat, out double lon)
		{

			CalcGeometry();

			if(mGI.ViewTileSizePixels == 0) {
				// We can't calculate it!
				lat = lon = 0;
				return;
			}

			lon = mLon + mScale * (x - Width / 2);

			double offset = (double)(y - mGI.ViewYTileYPos) / mGI.ViewTileSizePixels;
			double ty = mGI.CentreTileNumY + offset;
			double n = Math.PI - ((2.0 * Math.PI * ty) / Math.Pow(2.0, mGI.ViewTileZoom));
			lat = (180.0 / Math.PI * Math.Atan(Math.Sinh(n)));
		}

		/// <summary>
		/// Get the view position in the client area of the given latitude and
		/// longitude.
		/// </summary>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		/// <param name="x">The X position</param>
		/// <param name="y">The Y position</param>
		private void LatLonToClientPos(double lat, double lon, out int x, out int y)
		{
			CalcGeometry();
			x = Width / 2 + (int)((lon - mLon) / mScale);
			double ty = ((1.0 - Math.Log(Math.Tan(lat * Math.PI / 180.0) +
				1.0 / Math.Cos(lat * Math.PI / 180.0)) / Math.PI) / 2.0 * Math.Pow(2.0, mGI.ViewTileZoom));
			y = mGI.ViewYTileYPos + (int)((ty - mGI.CentreTileNumY) * mGI.ViewTileSizePixels);
		}

		/// <summary>
		///Used to store calculated view geometry information. 
		/// </summary>
		private class GeometryInfo
		{
			// These five fields record the parameters used to calculate
			// the information...
			public int Width = -1;
			public int Height;
			public double Lat;
			public double Lon;
			public double Scale;

			// The remaining fields are the calculated information...
			public int CentreTileNumX;	// Tile number (X) in centre
			public int CentreTileNumY;	// Tile number (Y) in centre
			public int ViewYTileYPos;
			public int ViewTileSizePixels;
			public int ViewTileZoom;
			public double CentreTileLonT;
			public double CentreTileLonB;

		}
		private GeometryInfo mGI = new GeometryInfo();

		private void CalcGeometry()
		{

			// We don't need to do anything if the information we already have
			// is valid for the current view...			
			if(Width == mGI.Width &&
				Height == mGI.Height &&
				mLat == mGI.Lat &&
				mLon == mGI.Lon &&
				mScale == mGI.Scale)
				return;

			mGI.Width = Width;
			mGI.Height = Height;
			mGI.Lat = mLat;
			mGI.Lon = mLon;
			mGI.Scale = mScale;

			// Figure out what tile zoom we want...
			// We want this many degrees longitude per tile...
			double lonpertilewanted = 256 * mScale;
			// Meaning we want this many tiles across in total.
			int tilesnumwanted = (int)(360.0 / lonpertilewanted);
			// Which corresponds to this zoom level...
			int tilezoom = (int)(Math.Log(tilesnumwanted) / Math.Log(2));
			if(tilezoom > 18)
				tilezoom = 18;
			mGI.ViewTileZoom = tilezoom;

			// Get the tile x/y at the centre of the view...
			GetTileXYFromLatLon(mLat, mLon, tilezoom, out mGI.CentreTileNumX, out mGI.CentreTileNumY);

			// Get the lat lon at the top left corner of that tile...
			double lat, lon;
			GetLatLonFromTileXY(mGI.CentreTileNumX, mGI.CentreTileNumY, tilezoom, out lat, out lon);
			mGI.CentreTileLonT = lon;

			// Get the lat lon at the bottom left corner as well...
			double lat2, lon2;
			GetLatLonFromTileXY(mGI.CentreTileNumX + 1, mGI.CentreTileNumY + 1, tilezoom, out lat2, out lon2);
			mGI.CentreTileLonB = lon2;

			// Calculate longitude degrees per tile...
			double lonpertile = 360.0 / (Math.Pow(2, tilezoom));

			// Calculate longitude degrees per pixel on the tile...
			double lonpertilepixel = lonpertile / 256;

			// Calculate the size (pixels) that tiles will be scaled to
			// when drawn...
			mGI.ViewTileSizePixels = (int)(256 * lonpertilepixel / mScale);

			mGI.ViewYTileYPos = (int)(Height / 2 - ((double)mGI.ViewTileSizePixels * ((mLat - lat) / (lat2 - lat))));

		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			if(!DesignMode) {
				try {

					CalcGeometry();

					// Find where to draw the centre tile...
					int drawx, drawy;
					drawx = (int)(Width / 2 - ((double)mGI.ViewTileSizePixels * ((mLon - mGI.CentreTileLonT) / (mGI.CentreTileLonB - mGI.CentreTileLonT))));
					drawy = mGI.ViewYTileYPos;

					int x = mGI.CentreTileNumX;
					int y = mGI.CentreTileNumY;

					// Move the position back to the top-leftmost tile we need to
					// draw...
					while(drawx>=0) {
						drawx -= mGI.ViewTileSizePixels;
						x--;
					}
					while(drawy>=0) {
						drawy -= mGI.ViewTileSizePixels;
						y--;
					}
	
					// Draw all the tiles...
					int curdrawx;
					int curx;
					while(drawy<Height) {
						curdrawx = drawx;
						curx = x;
						while(curdrawx<Width) {
							Image img = mTileManager.GetTile(curx, y, mGI.ViewTileZoom);
							if(img != null)
								e.Graphics.DrawImage(img, curdrawx, drawy,
									mGI.ViewTileSizePixels + 1, mGI.ViewTileSizePixels + 1);
							curdrawx += mGI.ViewTileSizePixels;
							curx++;
						}
						drawy += mGI.ViewTileSizePixels;
						y++;
					}

					// Draw markers...
					foreach(Marker m in mMarkers) {
						int mx, my;
						LatLonToClientPos(m.lat, m.lon, out mx, out my);
						Pen pen = new Pen(Color.Red);
						e.Graphics.DrawEllipse(pen, mx - 3, my - 3, 7, 7);
						pen.Dispose();
					}

					// Draw tracks...
					foreach(Track t in mTracks) {
						int px = 0, py = 0;	// Initialised only to pacify the compiler
						int tx, ty;
						bool first = true;
						Pen pen = new Pen(Color.Blue);
						foreach(TrackPoint p in t.Points) {
							LatLonToClientPos(p.Lat, p.Lon, out tx, out ty);
							if(!first) {
								e.Graphics.DrawLine(pen, new Point(px, py),
								                    	new Point(tx, ty));
							}
							px = tx;
							py = ty;
							first = false;
						}
						pen.Dispose();
					}

					// Highlight something if necessary...
					if(mHighlightedEntity != null) {
						Pen pen;
						int mx, my;
						switch(mHighlightedEntity.Type) {
						case EntityType.marker:
							pen = new Pen(Color.Blue, 3);
							LatLonToClientPos(mHighlightedEntity.Marker.lat, mHighlightedEntity.Marker.lon, out mx, out my);
							e.Graphics.DrawEllipse(pen, mx - 6, my - 6, 13, 13);
							pen.Dispose();
							break;
						case EntityType.trackpoint:
							pen = new Pen(Color.Orange, 2);
							foreach(TrackPoint tp in mHighlightedEntity.Track.Points) {
								if(tp.Time == mHighlightedEntity.Time) {
									LatLonToClientPos(tp.Lat, tp.Lon, out mx, out my);
									e.Graphics.DrawEllipse(pen, mx - 4, my - 4, 9, 9);
								}
							}
							pen.Dispose();
							break;
						}
					}

				} catch(Exception ex) {
					Diags("Drawing Exception: " + ex.Message);
				}

			}
	
		}

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing) {
				if(components != null)
					components.Dispose();
				if(mTileManager != null)
					mTileManager.NewDataAvailable -= NewTileDataAvailable;
			}
			base.Dispose(disposing);
		}

		void Timer1Tick(object sender, EventArgs e)
		{
			if(mUpdateView) {
				Invalidate();
				mUpdateView = false;
			}
		}

		/// <summary>
		/// Types of entity that can be found by GetEntityAtClientPos. 
		/// </summary>
		public enum EntityType
		{
			latlon,
			marker,
			trackpoint
		}

		public class EntityInfo
		{
			public EntityType Type;
			public double Lat;
			public double Lon;
			public Marker Marker;
			public Track Track;
			public DateTime Time;

			public bool Equals(EntityInfo e2)
			{
				if(this.Type != e2.Type)
					return false;
				switch(this.Type) {
				case EntityType.latlon:
					if(Lat != e2.Lat || Lon != e2.Lon)
						return false;
					break;
				case EntityType.marker:
					if(Marker != e2.Marker)
						return false;
					break;
				case EntityType.trackpoint:
					if(Track != e2.Track)
						return false;
					break;
				default:
					throw new ApplicationException("Unsupported entity type");
				}
				return true;
			}
		}

		/// <summary>
		/// Get the 'entity' at the given client position.
		/// </summary>
		/// <param name="x">The client X position.</param>
		/// <param name="y">The client Y position.</param>
		/// <returns>An EntityInfo instance describing the entity.</returns>
		private EntityInfo GetEntityAtClientPos(int x, int y)
		{
			EntityInfo ei = new EntityInfo();

			int tx, ty;
			foreach(Marker m in mMarkers) {
				LatLonToClientPos(m.lat, m.lon, out tx, out ty);
				if(Math.Abs(x - tx) < 4 && Math.Abs(y - ty) < 4) {
					ei.Marker = m;
					ei.Type = EntityType.marker;
					return ei;
				}
			}
			foreach(Track t in mTracks) {
				foreach(TrackPoint tp in t.Points) {
					LatLonToClientPos(tp.Lat, tp.Lon, out tx, out ty);
					if(Math.Abs(x - tx) < 3 && Math.Abs(y - ty) < 3) {
						ei.Track = t;
						ei.Time = tp.Time;
						ei.Type = EntityType.trackpoint;
						return ei;
					}
				}
			}

			ClientPosToLatLon(x, y, out ei.Lat, out ei.Lon);
			ei.Type = EntityType.latlon;
			return ei;

		}

		private bool mMouseMoving = false;
		private int mMouseMovingFromX;
		private int mMouseMovingFromY;
		private int mMouseMovingStartX;
		private int mMouseMovingStartY;

		void MapControlMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mMouseMoving = true;
			mMouseMovingFromX = e.X;
			mMouseMovingFromY = e.Y;
			mMouseMovingStartX = e.X;
			mMouseMovingStartY = e.Y;
		}

		void MapControlMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mMouseMoving = false;
			if(e.X == mMouseMovingStartX && e.Y == mMouseMovingStartY) {

				EntityInfo ei = GetEntityAtClientPos(e.X, e.Y);
				switch(ei.Type) {
				case EntityType.trackpoint:
					if(TrackPointClicked != null)
						TrackPointClicked(ei.Track, ei.Time);
					break;
				case EntityType.marker:
					if(MarkerClicked != null)
						MarkerClicked(ei.Marker);
					break;
				case EntityType.latlon:
					if(LocationClicked != null)
						LocationClicked(ei.Lat, ei.Lon);
					break;
				}

			}
		}

		void MapControlMouseLeave(object sender, System.EventArgs e)
		{
			mMouseMoving = false;
			mHighlightedEntity = null;
			Invalidate();
		}

		void MapControlMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(mMouseMoving) {
				int movedx = e.X - mMouseMovingFromX;
				int movedy = e.Y - mMouseMovingFromY;
				if(movedx != 0 || movedy != 0) {
					mLat += mScale * movedy;
					mLon -= mScale * movedx;
					Invalidate();
					RaiseViewChanged();
				}
				mMouseMovingFromX = e.X;
				mMouseMovingFromY = e.Y;
			} else {
				if(mHighlightEntities) {
					EntityInfo ei = GetEntityAtClientPos(e.X, e.Y);
					EntityInfo newh;
					if(ei.Type == EntityType.marker || ei.Type == EntityType.trackpoint) {
						newh = ei;
					} else {
						newh = null;
					}
					if(mHighlightedEntity == null) {
						if(newh != null) {
							mHighlightedEntity = newh;
							Invalidate();
						}
					} else {
						if(newh == null) {
							mHighlightedEntity = null;
							Invalidate();
						} else {
							if(!newh.Equals(mHighlightedEntity)) {
								mHighlightedEntity = newh;
								Invalidate();
							}
						}
					}
				}
			}
		}

	}
}
