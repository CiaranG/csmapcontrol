//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010-2013, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 
using System;
using System.Collections.Generic;
using System.Xml;
using System.Drawing;
using System.Text;
using System.IO;

namespace csmapcontrol
{
	public class TrackPoint : ICloneable
	{
		public double Lat;
		public double Lon;
		public double Elevation;
		public DateTime Time;

		public object Clone()
		{
			TrackPoint copy = new TrackPoint();
			copy.Lat = Lat;
			copy.Lon = Lon;
			copy.Elevation = Elevation;
			copy.Time = Time;
			return copy;
		}

	}

	public class Track
	{

		public List<TrackPoint> Points;
		public string Name;

		/// <summary>
		/// Create a new empty Track.
		/// </summary>
		public Track()
		{
			Points = new List<TrackPoint>();
		}

		/// <summary>
		/// Create a new Track by loading from a GPX file.
		/// </summary>
		/// <param name="filespec">The filespec of a GPX file.</param>
		public Track(string gpxfile)
		{
			Points = new List<TrackPoint>();
			XmlDocument x = new XmlDocument();
			x.Load(gpxfile);
			if(x.DocumentElement.Name != "gpx")
				throw new ApplicationException("Document element should be 'gpx'");
			string ns = "http://www.topografix.com/GPX/1/0";
			string ver = x.DocumentElement.GetAttribute("version");
			if(ver != null && ver == "1.1")
				ns = "http://www.topografix.com/GPX/1/1";
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(x.NameTable);
			nsmgr.AddNamespace("gpx", ns);
			if(x.GetElementsByTagName("name").Count > 0)
				Name = x.GetElementsByTagName("name") [0].InnerText;
			else
				Name = Path.GetFileNameWithoutExtension(gpxfile);
			foreach(XmlNode point in x.SelectNodes("//gpx:trkpt",nsmgr)) {
				TrackPoint p = new TrackPoint();
				p.Lat = XmlConvert.ToDouble(point.SelectSingleNode("@lat").Value);
				p.Lon = XmlConvert.ToDouble(point.SelectSingleNode("@lon").Value);
				p.Elevation = XmlConvert.ToDouble(point.SelectSingleNode("gpx:ele", nsmgr).InnerText);
				p.Time = DateTime.Parse(point.SelectSingleNode("gpx:time", nsmgr).InnerText).ToUniversalTime();
				Points.Add(p);
			}
		}

		/// <summary>
		/// Get a GPX representation of the track data.
		/// </summary>
		/// <returns>GPX data. (UTF-8 encoded)</returns>
		public string ToGPX()
		{
			const string gpx = "http://www.topografix.com/GPX/1/1";
			StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.UTF8;
			using(XmlWriter writer=XmlWriter.Create(sb, settings)) {
				writer.WriteStartElement("gpx", gpx);
				writer.WriteAttributeString("version", "1.1");
				writer.WriteAttributeString("creator", "CSMapControl");

				Bounds b = CalculateBounds();
				writer.WriteStartElement("bounds", gpx);
				writer.WriteAttributeString("minlat", XmlConvert.ToString(b.MinLat));
				writer.WriteAttributeString("maxlat", XmlConvert.ToString(b.MaxLat));
				writer.WriteAttributeString("minlon", XmlConvert.ToString(b.MinLon));
				writer.WriteAttributeString("maxlon", XmlConvert.ToString(b.MaxLon));
                writer.WriteEndElement();

				writer.WriteStartElement("trk", gpx);
				writer.WriteStartElement("name", gpx);
				writer.WriteValue(Name);
				writer.WriteEndElement();

				writer.WriteStartElement("trkseg", gpx);
				foreach(TrackPoint p in Points) {
					writer.WriteStartElement("trkpt", gpx);
					writer.WriteAttributeString("lat", XmlConvert.ToString(p.Lat));
					writer.WriteAttributeString("lon", XmlConvert.ToString(p.Lon));
					writer.WriteStartElement("ele", gpx);
					writer.WriteValue(XmlConvert.ToString(p.Elevation));
					writer.WriteEndElement();
					writer.WriteStartElement("time", gpx);
					writer.WriteValue(p.Time.ToString("s") + "Z");
					writer.WriteEndElement();
					writer.WriteEndElement();
				}
				
				writer.WriteEndElement();
			}
			string s = sb.ToString();
            return s.Replace(Encoding.Unicode.WebName, Encoding.UTF8.WebName);

		}
		
		/// <summary>
		/// The start time of the track. Returns DateTime.MinValue if the
		/// track has no points.
		/// </summary>
		public DateTime StartTime
		{
			get {
				if(Points.Count == 0)
					return DateTime.MinValue;
				return Points [0].Time;
			}
		}

		/// <summary>
		/// The end time of the track. Returns DateTime.MinValue if the
		/// track has no points.
		/// </summary>
		public DateTime EndTime
		{
			get {
				if(Points.Count == 0)
					return DateTime.MinValue;
				return Points [Points.Count - 1].Time;
			}
		}

		/// <summary>
		/// Split this Track at the specified time.
		/// </summary>
		/// <param name="splittime">The time to split at.</param>
		/// <returns>An array containing two new Track objects, the first being everything
		/// from this Track up to and including splittime, and the second being the
		/// remainder.</returns>
		public Track[] Split(DateTime splittime)
		{
			Track[] retval = new Track[2];
			retval [0] = new Track();
			retval [1] = new Track();
			retval [0].Name = Name + " Part 1";
			retval [1].Name = Name + " Part 2";
			foreach(TrackPoint p in Points)
				if(p.Time <= splittime)
					retval [0].Points.Add((TrackPoint)p.Clone());
				else
					retval [1].Points.Add((TrackPoint)p.Clone());
			return retval;
		}

		/// <summary>
		/// Remove the trackpoint at the given time.
		/// </summary>
		/// <param name="removetime">The time at which the point will be removed. Any
		/// points at this exact time will be removed.</param>
		public void RemovePoint(DateTime removetime)
		{
			List<TrackPoint > toremove = new List<TrackPoint>();
			foreach(TrackPoint p in Points)
				if(p.Time == removetime)
					toremove.Add(p);
			foreach(TrackPoint p in toremove)
				Points.Remove(p);
		}

        /// <summary>
        /// Remove the trackpoints before the given time.
        /// </summary>
        /// <param name="removetime">The time before which any points will be
        /// removed.</param>
        public void RemovePointsBefore(DateTime removetime)
        {
            List<TrackPoint > toremove = new List<TrackPoint>();
            foreach(TrackPoint p in Points)
                if(p.Time < removetime)
                    toremove.Add(p);
            foreach(TrackPoint p in toremove)
                Points.Remove(p);
        }

        /// <summary>
        /// Remove the trackpoints after the given time.
        /// </summary>
        /// <param name="removetime">The time after which any points will be
        /// removed.</param>
        public void RemovePointsAfter(DateTime removetime)
        {
            List<TrackPoint > toremove = new List<TrackPoint>();
            foreach(TrackPoint p in Points)
                if(p.Time > removetime)
                    toremove.Add(p);
            foreach(TrackPoint p in toremove)
                Points.Remove(p);
        }

        public class Bounds
		{
			public double MinLat;
			public double MinLon;
			public double MaxLat;
			public double MaxLon;
		}

		/// <summary>
		/// Calculate the bounds of this track.
		/// </summary>
		/// <returns>The bounds of the track, or null if the track contains no
		/// data at all.</returns>
		public Bounds CalculateBounds()
		{
			Bounds bounds = null;
			foreach(TrackPoint p in Points) {
				if(bounds == null) {
					bounds = new Bounds();
					bounds.MinLat = bounds.MaxLat = p.Lat;
					bounds.MinLon = bounds.MaxLon = p.Lon;
				} else {
					if(p.Lat < bounds.MinLat)
						bounds.MinLat = p.Lat;
					if(p.Lat > bounds.MaxLat)
						bounds.MaxLat = p.Lat;
					if(p.Lon < bounds.MinLon)
						bounds.MinLon = p.Lon;
					if(p.Lon > bounds.MaxLon)
						bounds.MaxLon = p.Lon;
				}
			}
			return bounds;
		}

		/// <summary>
		/// Calculate the length of the track.
		/// </summary>
		/// <returns>The length in metres.</returns>
		public double CalculateLength()
		{
			TrackPoint last = null;
			double dist = 0;
			foreach(TrackPoint p in Points) {
				if(last != null) {
					dist += distance(last.Lat, last.Lon, p.Lat, p.Lon);
				}
				last = p;
			}
			return dist;
		}

		/// <summary>
		/// Calculate geodesic distance (in m) between two points specified by
		/// latitude/longitude (in numeric degrees) using Vincenty inverse
		/// formula for ellipsoids. 
		///
		/// Code based on a Javascript version by © 2002-2008 Chris Veness. 
		///
		/// That code contains the following: "You are welcome to re-use these scripts 
		/// [without any warranty express or implied] provided you retain my copyright notice 
		/// and when possible a link to my website (under a LGPL license)."
		///
		/// The originating web page: http://www.movable-type.co.uk/scripts/latlong-vincenty.html
		///
		/// In turn, this python conversion was done by Maarten Sneep and released
		/// under the GPL V2 at http://www.xs4all.nl/~msneep/software/whereabouts-readme.html
		/// 
		/// In turn, it was converted to C# by Ciaran Gultnieks.
		/// </summary>
		/// <param name="lat1">Latitude of first point.</param>
		/// <param name="lon1">Longitude of first point.</param>
		/// <param name="lat2">Latitude of second point.</param>
		/// <param name="lon2">Longitude of second point.</param>
		/// <returns>The distance in metres.</returns>
		private double distance(double lat1, double lon1, double lat2, double lon2)
		{
			
			double a = 6378137;
			// in m
			double b = 6356752.3142;
			// in m
			double f = 1 / 298.257223563;
			double deg2rad = Math.PI / 180;
			lat1 *= deg2rad;
			lon1 *= deg2rad;
			lat2 *= deg2rad;
			lon2 *= deg2rad;
			
			double L = lon2 - lon1;
			double U1 = Math.Atan((1 - f) * Math.Tan(lat1));
			double U2 = Math.Atan((1 - f) * Math.Tan(lat2));
			double sinU1 = Math.Sin(U1);
			double cosU1 = Math.Cos(U1);
			double sinU2 = Math.Sin(U2);
			double cosU2 = Math.Cos(U2);
			double Lambda = L;
			double LambdaP = 2 * Math.PI;
			int iterLimit = 20;
			
			double sinLambda, cosLambda, sinSigma = 0, cosSigma = 0;
			double cosSqAlpha = 0, sinAlpha, cos2SigmaM = 0;
			double C, sigma = 0;
			
			while(Math.Abs (Lambda - LambdaP) > 1e-12 && iterLimit > 0) {
				sinLambda = Math.Sin(Lambda);
				cosLambda = Math.Cos(Lambda);
				sinSigma = Math.Sqrt(Math.Pow(cosU2 * sinLambda, 2) + Math.Pow(cosU1 * sinU2 - sinU1 * cosU2 * cosLambda, 2));
				if(sinSigma == 0)
					return 0;
				cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
				sigma = Math.Atan2(sinSigma, cosSigma);
				sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
				cosSqAlpha = 1 - Math.Pow(sinAlpha, 2);
				try {
					
					cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
				} catch(Exception) {
					cos2SigmaM = 0;
				}
				C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
				LambdaP = Lambda;
				Lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * Math.Pow(cos2SigmaM, 2))));
				iterLimit -= 1;
				
			}
			
			if(iterLimit == 0)
				return 0;
			
			double uSq = cosSqAlpha * (Math.Pow(a, 2) - Math.Pow(b, 2)) / Math.Pow(b, 2);
			double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
			double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
			double deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * Math.Pow(cos2SigmaM, 2)) - B / 6 * cos2SigmaM * (-3 + 4 * Math.Pow(sinSigma, 2)) * (-3 + 4 * Math.Pow(cos2SigmaM, 2))));
			return b * A * (sigma - deltaSigma);
			
		}
		
	}
}
