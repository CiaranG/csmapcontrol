//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010-2012, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 

namespace MapDemo
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.mapControl1 = new csmapcontrol.MapControl();
			this.txtLat = new System.Windows.Forms.TextBox();
			this.txtLon = new System.Windows.Forms.TextBox();
			this.scrScale = new System.Windows.Forms.HScrollBar();
			this.SuspendLayout();
			// 
			// mapControl1
			// 
			this.mapControl1.Location = new System.Drawing.Point(2, 2);
			this.mapControl1.Size = new System.Drawing.Size(432, 303);
			this.mapControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.mapControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.mapControl1.Latitude = 53.9753;
			this.mapControl1.Longitude = -1.7017;
			this.mapControl1.LonScale = 0.00390625;
			this.mapControl1.Name = "mapControl1";
			this.mapControl1.TabIndex = 0;
			// 
			// txtLat
			// 
			this.txtLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtLat.Location = new System.Drawing.Point(2, 311);
			this.txtLat.Name = "txtLat";
			this.txtLat.ReadOnly = true;
			this.txtLat.Size = new System.Drawing.Size(100, 20);
			this.txtLat.TabIndex = 1;
			// 
			// txtLon
			// 
			this.txtLon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtLon.Location = new System.Drawing.Point(107, 311);
			this.txtLon.Name = "txtLon";
			this.txtLon.ReadOnly = true;
			this.txtLon.Size = new System.Drawing.Size(100, 20);
			this.txtLon.TabIndex = 2;
			// 
			// scrScale
			// 
			this.scrScale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.scrScale.Location = new System.Drawing.Point(210, 311);
			this.scrScale.Name = "scrScale";
			this.scrScale.Size = new System.Drawing.Size(224, 20);
			this.scrScale.TabIndex = 3;
			this.scrScale.ValueChanged += new System.EventHandler(this.ScrScaleValueChanged);
			// 
			// MainForm
			// 
			this.ClientSize = new System.Drawing.Size(435, 333);
			this.Controls.Add(this.scrScale);
			this.Controls.Add(this.txtLon);
			this.Controls.Add(this.txtLat);
			this.Controls.Add(this.mapControl1);
			this.Name = "MainForm";
			this.Text = "MapDemo";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private System.Windows.Forms.HScrollBar scrScale;
		private System.Windows.Forms.TextBox txtLon;
		private System.Windows.Forms.TextBox txtLat;
		private csmapcontrol.MapControl mapControl1;
		
	}
}
