//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010-2014, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using csmapcontrol;

namespace MapDemo
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{

		public MainForm()
		{
			InitializeComponent();
		}

		void MainFormLoad(object sender, System.EventArgs e)
		{
			mapControl1.ViewChanged += MapViewChanged;
			mapControl1.LocationClicked += MapLocationClicked;
			mapControl1.TrackPointClicked += TrackPointClicked;
			scrScale.Maximum = MapControl.DiscreteScales.Length - 1;
			scrScale.Value = 20;
			mapControl1.LonScale = MapControl.DiscreteScales [scrScale.Value];
			txtLat.Text = mapControl1.Latitude.ToString();
			txtLon.Text = mapControl1.Longitude.ToString();

			// Add a Track for demo purposes...
			Track t = new Track("../SwinstyReservoir.gpx");
			mapControl1.Tracks.Add(t);

			mapControl1.HighlightEntities = true;
		}

		private void MapViewChanged(double lat, double lon, double scale)
		{
			txtLat.Text = lat.ToString();
			txtLon.Text = lon.ToString();
			int i = 0;
			for(; i<MapControl.DiscreteScales.Length-1; i++)
				if(MapControl.DiscreteScales [i] >= scale)
					break;
			scrScale.Value = i;
		}

		private void TrackPointClicked(Track t, DateTime time)
		{
		}

		private void MapLocationClicked(double lat, double lon)
		{
			Marker m = new Marker();
			m.lat = lat;
			m.lon = lon;
			mapControl1.Markers.Add(m);
			mapControl1.Invalidate();
		}

		void ScrScaleValueChanged(object sender, EventArgs e)
		{
			mapControl1.LonScale = MapControl.DiscreteScales [scrScale.Value];
		}

	}
}
